"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var chai_1 = require("chai");
require("mocha");
var system_1 = require("../src/system");
var entity_1 = require("../src/entity");
var ecs_1 = require("../src/ecs");
var TickTestSystem = /** @class */ (function (_super) {
    __extends(TickTestSystem, _super);
    function TickTestSystem() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    TickTestSystem.prototype.test = function (entity) {
        return entity.components.hasOwnProperty('tick');
    };
    TickTestSystem.prototype.updateEntity = function (entity, delta) {
        entity.components.tick.value++;
    };
    return TickTestSystem;
}(system_1.System));
var DeltaTestSystem = /** @class */ (function (_super) {
    __extends(DeltaTestSystem, _super);
    function DeltaTestSystem() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    DeltaTestSystem.prototype.test = function (entity) {
        return entity.components.hasOwnProperty('delta');
    };
    DeltaTestSystem.prototype.updateEntity = function (entity, delta) {
        entity.components.delta.value += delta;
    };
    return DeltaTestSystem;
}(system_1.System));
var TickComponent = {
    name: 'tick',
    data: {
        value: 0
    }
};
var DeltaComponent = {
    name: 'delta',
    data: {
        value: 0
    }
};
describe('ecs', function () {
    var ecs;
    var tickSystem;
    var deltaSystem;
    beforeEach(function () {
        ecs = new ecs_1.ECS();
        tickSystem = new TickTestSystem();
        deltaSystem = new DeltaTestSystem();
        ecs.addSystem(tickSystem);
        ecs.addSystem(deltaSystem);
    });
    it('only adds matching system to entity', function () {
        var entity = new entity_1.Entity([TickComponent]);
        ecs.addEntity(entity);
        ecs.update(0);
        chai_1.expect(entity.hasSystem(tickSystem)).to.equal(true);
        chai_1.expect(entity.hasSystem(deltaSystem)).to.equal(false);
    });
    it('updates entities correctly in a single update', function () {
        var entity = new entity_1.Entity([TickComponent, DeltaComponent]);
        ecs.addEntity(entity);
        ecs.update(66);
        chai_1.expect(entity.components.tick.value).to.equal(1);
        chai_1.expect(entity.components.delta.value).to.equal(66);
    });
});
//# sourceMappingURL=ecs.spec.js.map