import { expect } from 'chai';
import 'mocha';
import {System} from '../src/system'
import {Entity} from '../src/entity'
import {ECS} from "../src/ecs"

class TickTestSystem extends System {
  test(entity: Entity): boolean {
    return entity.components.hasOwnProperty('tick')
  }

  updateEntity(entity: Entity, delta: number): void {
    entity.components.tick.value++
  }
}

class DeltaTestSystem extends System {
  test(entity: Entity): boolean {
    return entity.components.hasOwnProperty('delta')
  }


  updateEntity(entity: Entity, delta: number): void {
    entity.components.delta.value += delta
  }
}

const TickComponent = {
  name: 'tick',
  data: {
    value: 0
  }
}

const DeltaComponent = {
  name: 'delta',
  data: {
    value: 0
  }
}

describe('ecs', () => {

  let ecs: ECS
  let tickSystem: TickTestSystem
  let deltaSystem: DeltaTestSystem

  beforeEach(() => {
    ecs = new ECS()
    tickSystem = new TickTestSystem()
    deltaSystem = new DeltaTestSystem()
    ecs.addSystem(tickSystem)
    ecs.addSystem(deltaSystem)
  })

  it('only adds matching system to entity', () => {
    const entity = new Entity([TickComponent])
    ecs.addEntity(entity)
    ecs.update(0)
    expect(entity.hasSystem(tickSystem)).to.equal(true)
    expect(entity.hasSystem(deltaSystem)).to.equal(false)
  })

  it('updates entities correctly in a single update', () => {
    const entity = new Entity([TickComponent, DeltaComponent])
    ecs.addEntity(entity)
    ecs.update(66)
    expect(entity.components.tick.value).to.equal(1)
    expect(entity.components.delta.value).to.equal(66)
  })

})