import {Entity} from './entity'
import {System} from './system'

export class ECS {
  private entities: Entity[] = []
  private entitiesSystemDirty: Entity[] = []
  private systems: System[] = []

  addEntity(entity: Entity): void {
    this.entities.push(entity)
    entity.referenceECS(this)
    this.addEntityToDirtyList(entity)
  }

  removeEntity(entity: Entity): void {
    const index = this.entities.indexOf(entity)

    if (index !== -1) {
      entity.dispose()
      this.removeEntityFromDirtyList(entity)
      this.entities.splice(index, 1)
    }
  }

  addSystem(system: System): void {
    this.systems.push(system)

    for (let i = 0; i < this.entities.length; i++) {
      const entity = this.entities[i]
      if (system.test(entity)) {
        system.addEntity(entity)
      }
    }
  }

  removeSystem(system: System): void {
    const index = this.systems.indexOf(system)
    if (index !== -1) {
      this.systems.splice(index, 1)
      system.dispose()
    }
  }

  fixDirtyEntities() {
    for (let i = 0; i < this.entitiesSystemDirty.length; i++) {
      const entity = this.entitiesSystemDirty[i]
      for (let j = 0; j < this.systems.length; j++) {
        const system = this.systems[j]

        const hasSystem = entity.hasSystem(system)
        const shouldHaveSystem = system.test(entity)

        if (!hasSystem && shouldHaveSystem) {
          system.addEntity(entity)
        } else if (hasSystem && !shouldHaveSystem) {
          system.removeEntity(entity)
        }
      }
      entity.markSystemsUndirty()
    }
  }

  update(delta: number): void {
    for (let i = 0; i < this.systems.length; i++) {
      this.fixDirtyEntities()
      this.systems[i].update(delta)
    }
  }

  addEntityToDirtyList(entity: Entity): void {
    this.entitiesSystemDirty.push(entity)
  }

  private removeEntityFromDirtyList(entity: Entity): void {
    const idx = this.entitiesSystemDirty.indexOf(entity)
    if (idx !== -1) {
      this.entitiesSystemDirty.splice(idx, 1)
    }
  }
}
