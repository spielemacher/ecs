import {Entity} from './entity'

export abstract class System {
  private entities: Entity[] = []

  addEntity(entity: Entity): void {
    entity.addSystem(this)
    this.entities.push(entity)
    this.enter(entity)
  }

  removeEntity(entity: Entity): void {
    const index = this.entities.indexOf(entity)
    if (index !== -1) {
      this.entities.splice(index, 1)
      this.exit(entity)
    }
  }

  update(delta: number) {
    this.preUpdate()
    for (let i = 0; i < this.entities.length; i++) {
      this.updateEntity(this.entities[i], delta)
    }
    this.postUpdate()
  }

  enter(entity: Entity): void {
  }

  preUpdate(): void {
  }

  updateEntity(entity: Entity, delta: number): void {
  }

  postUpdate(): void {
  }

  exit(entity: Entity): void {
  }

  abstract test(entity: Entity): boolean

  dispose(): void {
    for (let i = 0; this.entities.length; i++) {
      const entity = this.entities[0]
      this.exit(entity)
      entity.removeSystem(this)
    }
  }
}
