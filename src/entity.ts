import {ECS} from './ecs'
import {System} from './system'

export class Entity {
  public components: {[name: string]: any} = {}
  private markedSystemsDirty: boolean = false
  private ecs: ECS | null = null
  private systems: System[] = []


  constructor(components: {name: string, data: any}[]) {
    components.forEach(component => this.addComponent(component.name, Object.assign({}, component.data)))
  }

  referenceECS(ecs: ECS): void {
    this.ecs = ecs
  }

  addSystem(system: System): void {
    this.systems.push(system)
  }

  removeSystem(system: System): void {
    const index = this.systems.indexOf(system)
    if (index !== -1) {
      this.systems.splice(index, 1)
    }
  }

  hasSystem(system: System): boolean {
    return this.systems.indexOf(system) !== -1
  }

  addComponent(name: string, data: any): void {
    this.components[name] = data
    this.setSystemsDirty()
  }

  removeComponent(name: string): void {
    if (!this.components[name]) {
      return
    }
    delete this.components[name]
    this.setSystemsDirty()
  }

  markSystemsUndirty(): void {
    this.markedSystemsDirty = false
  }

  dispose(): void {
    for (let i = 0; i < this.systems.length; i++) {
      this.systems[i].removeEntity(this)
    }
  }

  private setSystemsDirty(): void {
    if (!this.markedSystemsDirty && this.ecs) {
      this.markedSystemsDirty = true
      this.ecs.addEntityToDirtyList(this)
    }
  }
}
