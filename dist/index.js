"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ecs_1 = require("./ecs");
exports.ECS = ecs_1.ECS;
var system_1 = require("./system");
exports.System = system_1.System;
var entity_1 = require("./entity");
exports.Entity = entity_1.Entity;
//# sourceMappingURL=index.js.map