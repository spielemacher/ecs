"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ECS = /** @class */ (function () {
    function ECS() {
        this.entities = [];
        this.entitiesSystemDirty = [];
        this.systems = [];
    }
    ECS.prototype.addEntity = function (entity) {
        this.entities.push(entity);
        entity.referenceECS(this);
        this.addEntityToDirtyList(entity);
    };
    ECS.prototype.removeEntity = function (entity) {
        var index = this.entities.indexOf(entity);
        if (index !== -1) {
            entity.dispose();
            this.removeEntityFromDirtyList(entity);
            this.entities.splice(index, 1);
        }
    };
    ECS.prototype.addSystem = function (system) {
        this.systems.push(system);
        for (var i = 0; i < this.entities.length; i++) {
            var entity = this.entities[i];
            if (system.test(entity)) {
                system.addEntity(entity);
            }
        }
    };
    ECS.prototype.removeSystem = function (system) {
        var index = this.systems.indexOf(system);
        if (index !== -1) {
            this.systems.splice(index, 1);
            system.dispose();
        }
    };
    ECS.prototype.fixDirtyEntities = function () {
        for (var i = 0; i < this.entitiesSystemDirty.length; i++) {
            var entity = this.entitiesSystemDirty[i];
            for (var j = 0; j < this.systems.length; j++) {
                var system = this.systems[j];
                var hasSystem = entity.hasSystem(system);
                var shouldHaveSystem = system.test(entity);
                if (!hasSystem && shouldHaveSystem) {
                    system.addEntity(entity);
                }
                else if (hasSystem && !shouldHaveSystem) {
                    system.removeEntity(entity);
                }
            }
            entity.markSystemsUndirty();
        }
    };
    ECS.prototype.update = function (delta) {
        for (var i = 0; i < this.systems.length; i++) {
            this.fixDirtyEntities();
            this.systems[i].update(delta);
        }
    };
    ECS.prototype.addEntityToDirtyList = function (entity) {
        this.entitiesSystemDirty.push(entity);
    };
    ECS.prototype.removeEntityFromDirtyList = function (entity) {
        var idx = this.entitiesSystemDirty.indexOf(entity);
        if (idx !== -1) {
            this.entitiesSystemDirty.splice(idx, 1);
        }
    };
    return ECS;
}());
exports.ECS = ECS;
//# sourceMappingURL=ecs.js.map