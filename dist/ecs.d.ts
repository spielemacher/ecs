import { Entity } from './entity';
import { System } from './system';
export declare class ECS {
    private entities;
    private entitiesSystemDirty;
    private systems;
    addEntity(entity: Entity): void;
    removeEntity(entity: Entity): void;
    addSystem(system: System): void;
    removeSystem(system: System): void;
    fixDirtyEntities(): void;
    update(delta: number): void;
    addEntityToDirtyList(entity: Entity): void;
    private removeEntityFromDirtyList;
}
