"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var System = /** @class */ (function () {
    function System() {
        this.entities = [];
    }
    System.prototype.addEntity = function (entity) {
        entity.addSystem(this);
        this.entities.push(entity);
        this.enter(entity);
    };
    System.prototype.removeEntity = function (entity) {
        var index = this.entities.indexOf(entity);
        if (index !== -1) {
            this.entities.splice(index, 1);
            this.exit(entity);
        }
    };
    System.prototype.update = function (delta) {
        this.preUpdate();
        for (var i = 0; i < this.entities.length; i++) {
            this.updateEntity(this.entities[i], delta);
        }
        this.postUpdate();
    };
    System.prototype.enter = function (entity) {
    };
    System.prototype.preUpdate = function () {
    };
    System.prototype.updateEntity = function (entity, delta) {
    };
    System.prototype.postUpdate = function () {
    };
    System.prototype.exit = function (entity) {
    };
    System.prototype.dispose = function () {
        for (var i = 0; this.entities.length; i++) {
            var entity = this.entities[0];
            this.exit(entity);
            entity.removeSystem(this);
        }
    };
    return System;
}());
exports.System = System;
//# sourceMappingURL=system.js.map