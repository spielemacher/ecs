import { Entity } from './entity';
export declare abstract class System {
    private entities;
    addEntity(entity: Entity): void;
    removeEntity(entity: Entity): void;
    update(delta: number): void;
    enter(entity: Entity): void;
    preUpdate(): void;
    updateEntity(entity: Entity, delta: number): void;
    postUpdate(): void;
    exit(entity: Entity): void;
    abstract test(entity: Entity): boolean;
    dispose(): void;
}
