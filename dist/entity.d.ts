import { ECS } from './ecs';
import { System } from './system';
export declare class Entity {
    components: {
        [name: string]: any;
    };
    private markedSystemsDirty;
    private ecs;
    private systems;
    constructor(components: {
        name: string;
        data: any;
    }[]);
    referenceECS(ecs: ECS): void;
    addSystem(system: System): void;
    removeSystem(system: System): void;
    hasSystem(system: System): boolean;
    addComponent(name: string, data: any): void;
    removeComponent(name: string): void;
    markSystemsUndirty(): void;
    dispose(): void;
    private setSystemsDirty;
}
