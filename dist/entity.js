"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Entity = /** @class */ (function () {
    function Entity(components) {
        var _this = this;
        this.components = {};
        this.markedSystemsDirty = false;
        this.ecs = null;
        this.systems = [];
        components.forEach(function (component) { return _this.addComponent(component.name, Object.assign({}, component.data)); });
    }
    Entity.prototype.referenceECS = function (ecs) {
        this.ecs = ecs;
    };
    Entity.prototype.addSystem = function (system) {
        this.systems.push(system);
    };
    Entity.prototype.removeSystem = function (system) {
        var index = this.systems.indexOf(system);
        if (index !== -1) {
            this.systems.splice(index, 1);
        }
    };
    Entity.prototype.hasSystem = function (system) {
        return this.systems.indexOf(system) !== -1;
    };
    Entity.prototype.addComponent = function (name, data) {
        this.components[name] = data;
        this.setSystemsDirty();
    };
    Entity.prototype.removeComponent = function (name) {
        if (!this.components[name]) {
            return;
        }
        delete this.components[name];
        this.setSystemsDirty();
    };
    Entity.prototype.markSystemsUndirty = function () {
        this.markedSystemsDirty = false;
    };
    Entity.prototype.dispose = function () {
        for (var i = 0; i < this.systems.length; i++) {
            this.systems[i].removeEntity(this);
        }
    };
    Entity.prototype.setSystemsDirty = function () {
        if (!this.markedSystemsDirty && this.ecs) {
            this.markedSystemsDirty = true;
            this.ecs.addEntityToDirtyList(this);
        }
    };
    return Entity;
}());
exports.Entity = Entity;
//# sourceMappingURL=entity.js.map